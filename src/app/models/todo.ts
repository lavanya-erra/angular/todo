export interface Todo {
  id?: string;
  description?: string;
  priority?: string;
  color?: string;
  completed?: boolean;
}

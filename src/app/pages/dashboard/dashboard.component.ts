import {Component, inject} from '@angular/core';
import {MatButton} from "@angular/material/button";
import {RouterLink} from "@angular/router";
import {Auth} from "@angular/fire/auth";
import {signout$} from "../../store/signals";
import {MatDialog} from '@angular/material/dialog';
import {TodoDialogComponent} from "../../dialogs/todo-dialog/todo-dialog.component";
import {collection, collectionData, deleteDoc, doc, Firestore, setDoc} from "@angular/fire/firestore";
import {Todo} from "../../models/todo";
import {Observable} from "rxjs";
import {AsyncPipe} from "@angular/common";
import {MatList, MatListItem, MatListOption, MatSelectionList} from "@angular/material/list";
import {MatCardModule} from "@angular/material/card";
import {MatDivider} from "@angular/material/divider";
import {MatIcon} from "@angular/material/icon";
import {MatChip} from "@angular/material/chips";
import {ChipComponent} from "../../atoms/chip/chip.component";
import {MatCheckbox} from "@angular/material/checkbox";

@Component({
  selector: 'app-dashboard',
  standalone: true,
  imports: [
    MatButton,
    RouterLink,
    AsyncPipe,
    MatSelectionList,
    MatListOption,
    MatCardModule,
    MatDivider,
    MatIcon,
    MatChip,
    ChipComponent,
    MatList,
    MatListItem,
    MatCheckbox
  ],
  templateUrl: './dashboard.component.html',
  styleUrl: './dashboard.component.scss'
})
export class DashboardComponent {
  showDelete = false;
  protected auth = inject(Auth);
  protected readonly signout$ = signout$;
  private matDialog = inject(MatDialog);
  private firestore = inject(Firestore);
  private collection = collection(this.firestore, 'todos');
  todos$ = collectionData(this.collection, {idField: 'id', serverTimestamps: 'estimate'}) as Observable<Todo[]>;

  openTodoDialog() {
    this.matDialog.open(TodoDialogComponent);
  }

  async deleteTodo(todo: Todo) {
    const docRef = doc(this.collection, todo.id)
    await deleteDoc(docRef);
  }

  async updateTodoStatus(todo: Todo) {
    const docRef = doc(this.collection, todo.id);
    await setDoc(docRef, {completed: !todo.completed}, {merge: true});
  }

  updateTodo(todo: Todo) {
    this.matDialog.open(TodoDialogComponent, {data: todo});
  }
}

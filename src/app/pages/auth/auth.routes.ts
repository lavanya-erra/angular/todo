import {Routes} from "@angular/router";

const children: Routes = [
  {
    path: 'login',
    loadComponent: () => import('./login/login.component').then(m => m.LoginComponent)
  },
  {
    path: 'register',
    loadComponent: () => import('./register/register.component').then(m => m.RegisterComponent)
  },
  {path: '', redirectTo: 'login', pathMatch: 'full'}
];

export const AUTH_ROUTES: Routes = [
  {
    path: '', children
  }
];

import {Component, inject} from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialogActions,
  MatDialogClose,
  MatDialogContent,
  MatDialogRef,
  MatDialogTitle
} from "@angular/material/dialog";
import {MatButton} from "@angular/material/button";
import {MatFormField, MatLabel} from "@angular/material/form-field";
import {MatInput} from "@angular/material/input";
import {MatOption, MatSelect} from "@angular/material/select";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {addDoc, collection, doc, Firestore, setDoc} from "@angular/fire/firestore";
import {showToast$} from "../../store/signals";
import {Todo} from "../../models/todo";

@Component({
  selector: 'app-todo-dialog',
  standalone: true,
  imports: [
    MatDialogTitle,
    MatDialogContent,
    MatDialogActions,
    MatButton,
    MatDialogClose,
    MatFormField,
    MatInput,
    MatLabel,
    MatSelect,
    MatOption,
    ReactiveFormsModule
  ],
  templateUrl: './todo-dialog.component.html',
  styleUrl: './todo-dialog.component.scss'
})
export class TodoDialogComponent {
  firestore = inject(Firestore);
  dialogRef: MatDialogRef<TodoDialogComponent> = inject(MatDialogRef);
  collection = collection(this.firestore, 'todos');

  todo: Todo = inject(MAT_DIALOG_DATA);

  formGroup = new FormGroup({
    description: new FormControl(this.todo?.description, [Validators.required]),
    priority: new FormControl(this.todo?.priority || 'Medium', [Validators.required])
  })

  async onSubmit() {
    const data: any = this.formGroup.value;
    switch (data.priority) {
      case 'High':
        data.color = 'warn';
        break;
      case 'Medium':
        data.color = 'accent';
        break;
      case 'Low':
        data.color = 'primary';
        break;
    }

    if (this.todo?.id) {
      const docRef = doc(this.collection, this.todo.id);
      await setDoc(docRef, data, {merge: true});
      showToast$.set('Document updated successfully!');
      this.dialogRef.close();
    } else {
      const docRef = await addDoc(this.collection, data);
      console.log('Document written with ID: ', docRef.id);
      showToast$.set('Document saved successfully!');
      this.dialogRef.close();
    }
  }
}
